%global _empty_manifest_terminate_build 0
Name:		python-incremental
Version:	24.7.2
Release:	1
Summary:	A small library versions your Python projects
License:	MIT
URL:		https://github.com/twisted/incremental
Source0:	https://files.pythonhosted.org/packages/source/i/incremental/incremental-%{version}.tar.gz
BuildArch:	noarch


%description
Incremental is a small library that versions your Python projects.

%package -n python3-incremental
Summary:	A small library versions your Python projects
Provides:	python-incremental = %{version}-%{release}
BuildRequires:	python3-devel python3-setuptools
%description -n python3-incremental
Incremental is a small library that versions your Python projects.

%package help
Summary:	Development documents and examples for incremental
Provides:	python3-incremental-doc
%description help
Incremental is a small library that versions your Python projects.

%prep
%autosetup -n incremental-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-incremental -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Oct 24 2024 jinshuaiyu <jinshuaiyu@kylinos.cn> - 24.7.2-1
- Update package to version 24.7.2
- Incremental could mis-identify that a project had opted in to version management.

* Thu Dec 15 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 22.10.0-1
- Update package to version 22.10.0

* Thu Jun 09 2022 SimpleUpdate Robot <tc@openeuler.org> - 21.3.0-1
- Upgrade to version 21.3.0

* Thu Mar 31 2022 yangping <yangping69@huawei.com> - 17.5.0-7
- Fix build error caused by py3.10+ wildcard

* Wed Oct 21 2020 leiju <leiju4@huawei.com> - 17.5.0-6
- remove python2 subpackage

* Fri Feb 21 2020 gulining<gulining1@huawei.com> - 17.5.0-5
- Package init.
